import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

//    Rozwijamy dotychczasową aplikację. Teraz - oczekujemy od użytkownika konkretnych danych, które będą zapisywane do
// pliku. Aplikacja będzie służyć jako formularz który zbiera dane (jak z ankiety). Zakładamy ankietę użytkownika która
// pyta użytkownika o następujące dane:
//
//    Podaj imie i nazwisko:
//    Podaj wiek:
//    Podaj płeć:
//
//    Kolejne pytania zadajemy dopiero wtedy, gdy użytkownik jest:
//    a) kobietą w wieku między 18-25
//    b) mężczyzną w wieku między 25-30
//
//            (powiedzmy że chodzi o jednakową dojrzałość osób biorących udział w ankiecie)
//    Wymyśl 3 pytania i zadaj je ankietowanemu.
//    Wszystkie pytania i odpowiedzi ankiety zapisz do pliku.
//    Otwieraj plik wyłącznie do dopisywania danych (jeśli istnieje).
//
//    Rozszerzenie:
//    Stwórz klasę modelu (Form) która przechowuje odpowiedzi na wszystkie pytania. Dodaj metodę toString, która będzie
// używana do wypisania obiektu do pliku.
//

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<String> lineList= new ArrayList<>();
        System.out.println("Enter name, surname:");
        Form form = new Form();
        try (PrintWriter write = new PrintWriter("anketa.txt");){

            String word= sc.nextLine();
            write.println(word);
            lineList.add(word);
            form.addAnswer(word);
            System.out.println("Age:");
            word = sc.nextLine();
            write.println(word);
            lineList.add(word);
            form.addAnswer(word);
            System.out.println("Sex:");
            word = sc.nextLine();
            write.println(word);
            lineList.add(word);
            form.addAnswer(word);
            if (lineList.get(2).equals("w")){
                if ((Integer.parseInt(lineList.get(1))>18) && (Integer.parseInt(lineList.get(1))<25)){
                    System.out.println("1");
                    form.addAnswer(sc.nextLine());
                    System.out.println("2");
                    form.addAnswer(sc.nextLine());
                    System.out.println("3");
                    form.addAnswer(sc.nextLine());

                }

            }
            if (lineList.get(2).equals("m")){
                if ((Integer.parseInt(lineList.get(1))>25) && (Integer.parseInt(lineList.get(1))<30)){
                    System.out.println("1");
                    form.addAnswer(sc.nextLine());
                    System.out.println("2");
                    form.addAnswer(sc.nextLine());
                    System.out.println("3");
                    form.addAnswer(sc.nextLine());
                }

            }
            write.println(form);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
