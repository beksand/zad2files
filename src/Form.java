import java.util.ArrayList;

public class Form {
    private ArrayList<String> answers= new ArrayList<>();

    public Form() {
    }

    public Form(ArrayList<String> answer) {
        this.answers = answer;
    }

    public ArrayList<String> getAnswer() {
        return answers;
    }

    public void setAnswer(ArrayList<String> answer) {
        this.answers = answer;
    }
    public void addAnswer(String answer){
        answers.add(answer);
    }

    @Override
    public String toString() {
        return "Anketa: " +
                "answers=" + answers +
                "";
    }
}
